var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        //var parentElement = document.getElementById(id);
       // var listeningElement = parentElement.querySelector('.listening');
        //var receivedElement = parentElement.querySelector('.received');

        //listeningElement.setAttribute('style', 'display:none;');
        //receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};


var myApp=angular.module('app',['ui.router','ui.bootstrap']);

myApp.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/");
  //
  // Now set up the states
  $stateProvider
    .state('center-finder', {
      url: "/center-finder",
      templateUrl: "partials/center-finder.html"
    }).state('/', {
      url: "/",
      templateUrl: "partials/splash.html"
    })
    .state('contact', {
      url: "/contact",
      templateUrl: "partials/contact.html"
    })
    .state('about-us', {
      url: "/about-us",
      templateUrl: "partials/about-us.html"
    })
    .state('feedback', {
      url: "/feedback",
      templateUrl: "partials/feedback.html"
    });
});

myApp.controller('splashController', ['$state','$timeout', function($state,$timeout){
    $timeout(function() {
         $state.transitionTo('center-finder');
    }, 7000);
   
}]);

myApp.controller('centerFinderController', ['$scope','$http', function($scope,$http){

function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      $scope.$apply(function () {
            $scope.centerFinder=JSON.parse(xhttp.responseText);

            
        });
    }
  };
  xhttp.open("GET", "https://services.tm.org/germinate-user-mgt/api/v1/centerinfo", true);
  xhttp.send();
}

loadDoc();


 /*$scope.getLocation = function(val) {
    return $scope.centerFinder
  };*/


     

   
}]);

function calculateDistance(latLngA, latLngB) {
        var distanceMeters = google.maps.geometry.spherical.computeDistanceBetween (latLngA, latLngB);
        var distanceMiles = parseInt(distanceMeters * 0.000621371192);
        var distanceKmeters = parseInt(distanceMeters / 1000);
        return [distanceMiles, distanceKmeters];
    }
    
    function addMarker(latitude,longitude,name,description) {
        var image = 'http://resources.tm.org/map/Tree_placemark-2_30.png';
        //var image = 'http://uk.tm.org/documents/12132/6205412/Tree_placemark_30.png';
        //var image = 'http://uat.uk.tmdev.org/documents/12132/197155/icon-marker.png';
        var myinfowindow = new google.maps.InfoWindow({ content: description });    
        
        marker = new google.maps.Marker({
            position: {lat: latitude, lng: longitude},
            map: map,
            title: name,
            icon: image,
            animation: google.maps.Animation.DROP,
            infowindow: myinfowindow
        });

        google.maps.event.addListener(marker, 'click', function() {
            this.infowindow.open(map, this);
        });
    
        
        infoWindows.push(myinfowindow);
        gmarkers.push(marker);
    }
    
    function deleteMarkers() {
        clearMarkers();
        gmarkers = [];
    }
    
    function clearMarkers() {
        setMapOnAll(null);
    }
    
    function setMapOnAll(map) {
        for (var i = 0; i < gmarkers.length; i++) {
            gmarkers[i].setMap(map);
        }
    }
    
    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
    
    function gotoCentre(myPoint){
        closeAllInfoWindows();
        map.setCenter(new google.maps.LatLng(gmarkers[myPoint].position.lat(), gmarkers[myPoint].position.lng()));
        gmarkers[myPoint]['infowindow'].open(map, gmarkers[myPoint]);
    }
    
    function closeAllInfoWindows() {
        for (var i=0;i<infoWindows.length;i++) {
            infoWindows[i].close();
        }
    }



var gmarkers = Array();
    var infoWindows = Array();

    var jsonObj;
    var jsonSorted = Array();
    var noMarkers = true;

    function getMapContent(currentLatLng) {
        console.log("data grabbed once");
        $JQ_191.ajax({
            type: 'GET',
            //url: 'http://services.tmdev.org/gcon-app/center-search.json',
            url: 'https://services.tm.org/germinate-user-mgt/api/v1/centerinfo',
            dataType: 'json',
            success: function (data) {
                jsonObj = data;
                superduperJson(jsonObj, currentLatLng);
            }
        });

    }

    function superduperJson(jsonObj, currentLatLng) {
        
        var jsonSorted = Array();
        // Build new JSON with distance from current location
        $JQ_191.each(jsonObj, function(index, element) {
            
            initialLocation = new google.maps.LatLng(element.latitude,element.longitude);
            var distance = calculateDistance(currentLatLng,initialLocation);
            distanceMiles = distance[0];
            distanceKilometers = distance[1];
            
            var obj = {
                organizationId: element.organizationId,
                displayName: element.displayName,
                website: element.website,
                centerChairman: element.centerChairman,
                telephoneMain: element.telephoneMain,

                websiteLink1Title: element.websiteLink1Title,
                websiteLink1Ref: element.websiteLink1Ref,
                websiteLink2Title: element.websiteLink2Title,
                websiteLink2Ref: element.websiteLink2Ref,
                
                longitude: element.longitude,
                latitude: element.latitude,
                distanceMiles: distanceMiles,
                distanceKilometers: distanceKilometers,
                instanceId: element.instanceId
            };
            jsonSorted.push(obj);
            

        }); // each
        jsonSorted.sort(function(a, b) {
            return parseFloat(a.distanceMiles) - parseFloat(b.distanceMiles);
        });
        
        deleteMarkers();
        
        $JQ_191('#map-centres').html('');
        // Now sort JSON for centres are ordered
        $JQ_191.each(jsonSorted, function(index, element) {     
            
                /*
                var id = element.id;
                var name = element.name;
                var description = element.description;
                var thumbnailUrl = element.thumbnailUrl;
                var markerIconUrl = element.markerIconUrl;
                var websiteUrl = element.websiteUrl;
                var fullAddress = element.fullAddress;
                var contactName = element.contactName;
                var contactEmail = element.contactEmail;
                var contactPhone = element.contactPhone;
                var longitude = element.longitude;  
                var latitude = element.latitude;*/
                
                var organizationId = element.organizationId;
                var displayName = element.displayName;
                var website = element.website;
                var centerChairman = element.centerChairman;
                var telephoneMain = element.telephoneMain;
                
                var websiteLink1Title = element.websiteLink1Title;
                var websiteLink1Ref = element.websiteLink1Ref;
                var websiteLink2Title = element.websiteLink2Title;
                var websiteLink2Ref = element.websiteLink2Ref;
                
                var distanceMiles = element.distanceMiles;  
                var distanceKilometers = element.distanceKilometers;
                
                var instanceId = Number(element.instanceId);
               var defaultCompanyId = 100;

                
                if( instanceId == defaultCompanyId) {

                    
                    infowindowContent = '<div class="infowindow-centre"><div class="name"><h3><a href="'+element.website+'" target="_blank">'+element.displayName+'</a></h3></div><div class="contactPhone"><label>Telephone:</label> '+element.telephoneMain+'</div><div class="contactName"><label>Contact:</label> '+element.centerChairman+'</div><div class="websiteUrl"><a href="'+element.website+'" target="_blank">Website</a></div><div class="websiteUrl"><a href="'+element.website+'" target="_blank">'+element.websiteLink1Title+'</a></div><div class="websiteUrl"><a href="'+element.websiteLink2Ref+'" target="_blank">'+element.websiteLink2Title+'</a></div></div>';
                    addMarker(element.latitude,element.longitude,element.displayName,infowindowContent);

                
                if(index < 11) {

                    centreContent = '<div class="centre-content" onClick="gotoCentre('+index+');"><div class="centre-id"><div class="inner">'+(index+1)+'.</div></div><div class="name">'+displayName+'</div><div class="distance"><div class="distance-miles">'+distanceMiles+' Miles</div><div class="distance-kilometers">'+distanceKilometers+' KM</div></div><div class="tidy"></div></div>';
                    $JQ_191('#map-centres').append(centreContent);
                }

                console.log(centreContent)
                 // alert('Hello ' + Liferay.ThemeDisplay.getCompanyId());
                
                //alert('instanceId = ' + instanceId);
                }
                
        }); // each
        noMarkers = false;
    
    
    } // superduperJson


(function($) {
 initMap = function(){

        // Defaults
        var myOptions = {
        zoom: 8,
        center: initialLocation
    };
        map = new google.maps.Map(document.getElementById('map'), myOptions); 
        
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        var searchedLat;
        var searchedLng;
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();
        
            if (places.length == 0) {
                return;
            }
        
            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];
        
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            
            places.forEach(function(place) {
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };
        
                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));
                
                searchedLat = place.geometry.location.lat();
                searchedLng = place.geometry.location.lng();
                currentLatLng = new google.maps.LatLng(searchedLat,searchedLng);
                superduperJson(jsonObj, currentLatLng)
                
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
        


        
        // Try W3C Geolocation (Preferred)      
        var initialLocation;
        var browserSupportFlag =  new Boolean();
        var defaultLat = '52.143602';
        var defaultLng = '-1.010742';
        console.log(navigator.geolocation)
        if(navigator.geolocation) {
            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function(position) {
                console.log(position)
                initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                map.setCenter(initialLocation);
                getMapContent(initialLocation);
                
            }, function() {
                console.log(browserSupportFlag)
                handleNoGeolocation(browserSupportFlag);
            });
            
        }
        // Browser doesn't support Geolocation
        else {
            console.log('else')
            browserSupportFlag = false;
            handleNoGeolocation(browserSupportFlag);
        }
        
        function handleNoGeolocation(errorFlag) {
            if (errorFlag == true) {
                console.log("Geolocation service failed.");
            } else {
                console.log("Your browser doesn't support geolocation.");
            }
            initialLocation = new google.maps.LatLng(defaultLat,defaultLng);
            map.setCenter(initialLocation);
            getMapContent(initialLocation);
        }
    
    } // initMap



    $JQ_191(function() {
        
        $JQ_191('.convert-distance').on('click', function(event) {
            event.preventDefault();
            if($JQ_191('.distance-miles').css('display') == 'block') {
                $JQ_191('.distance-miles').css('display', 'none');
                $JQ_191('.distance-kilometers').css('display','block');
            } else {
                $JQ_191('.distance-miles').css('display','block');
                $JQ_191('.distance-kilometers').css('display','none');
            }
        });

        $JQ_191('.pac-search').on('click', function(event) {
            event.preventDefault();
            var input = document.getElementById('pac-input');
            google.maps.event.trigger(input, 'focus')
            google.maps.event.trigger(input, 'keydown', {
                keyCode: 13
            });
        }); 
        
    });
    initMap();
})(jQuery)
